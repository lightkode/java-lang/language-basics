
import java.util.List;
import java.util.ArrayList;
// ArrayList
// add, 
// get by index 0, 1, 2
// remove by index
// remove by element
// for index
// for each

/**
[
    0 : bhubaneswar
    1 : cuttack
]
 */
public class App2{
    public static void main(String[] args){
        String name="John";
        List cities = new ArrayList();
        cities.add("bhubaneswar");
        cities.add("cuttack");
        cities.add("cuttack");

        System.out.println(cities);

        System.out.println(cities.get(0));
        System.out.println(cities.get(1));
        System.out.println(cities.get(2));

        int length = cities.size();
        for(int index=0; index < length; index++){
            System.out.println(index);
            System.out.println(cities.get(index));
        }

        for(Object elem:cities){
            System.out.println(elem);
        }

        cities.remove(0);
        System.out.println(cities);

        cities.remove("cuttack");
        System.out.println(cities);
       
    }
}