
import java.util.List;
import java.util.ArrayList;
public class App3{

    public static void main(String[] args){
        List cities = new ArrayList();
        cities.add("cuttack");
        cities.add("Bengaluru");
        cities.add(1, "Delhi");
        System.out.println(cities);

        List cities1 = new ArrayList();
        cities1.add("Jaipur");
        cities1.add("Hyderabad");
        System.out.println(cities1);
        
        List cities2 = new ArrayList(cities1);
        System.out.println(cities2);

        List cities3 = new ArrayList();
        cities3.addAll(cities);
        System.out.println(cities3);

        boolean result = cities1.contains("Jaipur");
        System.out.println(result);

        cities.clear();
        result = cities.isEmpty();
        System.out.println(result);
        
    }
}