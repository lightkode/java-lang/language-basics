
public class App1{

   public static void main(String[] args){

     A aRef = new B(); // aRef: it is a reference variable of type A
     int result = aRef.sum(10, 20);
     System.out.println(aRef);   
     System.out.println(result);
   }

   private static interface A {

    public int sum(int a, int b);

   }

   private static class B implements A{
      
      public int sum(int a, int b){
        return a + b;
      }

   }

}

